from behave import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager




@given(u'open root home page')
def step_impl(context):

    chrome_options = Options()

    chrome_options.add_argument('--headless')  # Ejecutar en modo headless
    chrome_options.add_argument('--disable-gpu')  # Deshabilitar la aceleración de gráficos para evitar problemas
    chrome_options.add_argument('--window-size=1920,1080')  # Tamaño de la ventana


    serviceChrome = Service(ChromeDriverManager().install())
    context.driver = webdriver.Chrome(service=serviceChrome, options=chrome_options)
    context.driver = webdriver.Remote('http://selenium__standalone-chrome:4444/wd/hub', options=webdriver.ChromeOptions())




    context.driver.get("https://rootstack.com/en")



@when(u'browse to contact us')
def step_impl(context):
    context.driver.find_element(By.XPATH,"//a[contains(@class,'nav-link small text-marine persists-active')]").click()



@when(u'fill name and lastName')
def step_impl(context):
    context.driver.execute_script("document.body.style.zoom = '65%'") #  # aplicar el zoom al 65% usando JavaScript
    context.driver.find_element(By.ID,"name").send_keys("user")
    context.driver.find_element(By.ID,"last_name").send_keys("user")


@then(u'user see service message')
def step_impl(context):

    text = context.driver.find_element(By.XPATH, "//h2[contains(@class,'text-3xl text-bold form--title text-center text-white')]").text
    assert text == "Find out more about our services"
    context.driver.close()

